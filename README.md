# Multi-site 5G slice inventory

Repository containing documentation and API defintion of the VITAL-5G Multi-site 5G slice inventory.  
The `OpenApi` can be found [here](https://studio.apicur.io/apis/81153/collaboration/accept/3b87a213-52c4-43cb-8f40-b8c2f04ec1ce).
The IP-addresses of the Slice inventory are: `172.28.18.101`(DEV) and `172.28.18.69`(PROD). (Both are running 24/7). 

## Southbound interfaces
Towards the local slice plugin on the testbed. 
### For the **URLLC** slice

1. To `add` a **URLLC** slice -> `POST` Request: 
- `172.28.18.101:8080/api/v1/sliceInventory/SB/reportSliceParameters/urllc`
`JSON` body:
```
{
  "sliceId": "AntwerpUrllc01",
  "site": "antwerp",
  "latency": 14,
  "jitter": 1,
  "expDataRateUL": 100,
  "expDataRateDL": 100,
  "trafficType": "TCP",
  "imsi":["imsi1", "imsi2", "imsi3"]
}
```
2. To `update` a **URLLC** slice -> `PUT` Request: 
- `172.28.18.101:8080/api/v1/sliceInventory/SB/reportSliceParameters/urllc`
`JSON` body:
```
{
  "sliceId": "AntwerpUrllc01",
  "site": "Antwerp",
  "latency": 16,
  "jitter": 1,
  "expDataRateUL": 100,
  "expDataRateDL": 100,
  "trafficType": "UDP",
  "imsi":["imsi1", "imsi2", "imsi3", "imsi4"]
}
```
3. To `delete` a **URLLC** slice -> `DELETE` Request: 
- `172.28.18.101:8080/api/v1/sliceInventory/SB/reportSliceParameters/urllc`
`JSON` body:
```
{
  "sliceId": "AntwerpUrllc01",
  "site": "Antwerp",
  "latency": 16,
  "jitter": 1,
  "expDataRateUL": 100,
  "expDataRateDL": 100,
  "trafficType": "UDP",
  "imsi":["imsi1", "imsi2", "imsi3", "imsi4"]
}
```

### For the **EMBB** slice
1. To `add` a **EMBB** slice -> `POST` Request: 
- `172.28.18.101:8080/api/v1/sliceInventory/SB/reportSliceParameters/embb`
`JSON` body:
```
{
  "sliceId": "AntwerpEmbb01",
  "site": "antwerp",
  "expDataRateUL": 200,
  "expDataRateDL": 200,
  "areaTrafficCapUL": 200,
  "areaTrafficCapDL": 200,
  "userDensity": 200,
  "userSpeed": 80,
  "trafficType": "TCP",
  "imsi":["imsi1", "imsi2", "imsi3"]
}
```
2. To `update` a **EMBB** slice -> `PUT` Request: 
- `172.28.18.101:8080/api/v1/sliceInventory/SB/reportSliceParameters/embb`
`JSON` body:
```
{
  "sliceId": "AntwerpEmbb01",
  "site": "Antwerp",
  "expDataRateUL": 400,
  "expDataRateDL": 200,
  "areaTrafficCapUL": 200,
  "areaTrafficCapDL": 200,
  "userDensity": 200,
  "userSpeed": 80,
  "trafficType": "UDP",
  "imsi":["imsi1", "imsi2", "imsi3", "imsi4"]
}
```
3. To `delete` a **EMBB** slice -> `DELETE` Request: 
- `172.28.18.101:8080/api/v1/sliceInventory/SB/reportSliceParameters/embb`
`JSON` body:
```
{
  "sliceId": "AntwerpEmbb01",
  "site": "Antwerp",
  "expDataRateUL": 400,
  "expDataRateDL": 200,
  "areaTrafficCapUL": 200,
  "areaTrafficCapDL": 200,
  "userDensity": 200,
  "userSpeed": 80,
  "trafficType": "UDP",
  "imsi":["imsi1", "imsi2", "imsi3", "imsi4"]
}
```

## Northbound interfaces
From the VITAL-5G Slice Invenotry Manager & Invenotry towareds the Service LCM. 
### For the **URLLC** slice

1. To `select` a **URLLC** slice -> `POST` Request: 
- `172.28.18.101:8080/api/v1/sliceInventory/NB/selectSlice/urllc`
`JSON` body:
```
{
  "latency": 14,
  "jitter": 1,
  "expDataRateUL": 100,
  "expDataRateDL": 100,
  "testbed": "Antwerp",
  "imsi":"imsi1"
}
```
In case there is a match for specific slice then the VITAL-5G Slice Manager & Invenotry will send out the `sliceUE/attach` request towards the corresponding local slice plugin (it requests from the VITAL-5G Multi-site inventory the `slicingAgentUrl`). In case the VITAL-5G Slice Manager & Invenotry fails to contact the local slice plugin of the specific testbed it will return in the responseEntity the HttpStatuscode `500` with the message: `Slice plugin failed to attach in a static way the imsi to the slice`. In the case that there wasn't a match for a slice, then the VITAL-5G Slice Manager & Invenotry will return in the responseEntity the HttpStatuscode `500` with the message: `No pre-provisioned slice in the db and the testbed doesn't support dynamic provisioning`. In the case that there weren't yet slices in the database then the VITAL-5G Slice Manager & Invenotry will return in the responseEntity the HttpStatuscode `500` with the message: `There are 0 slices on the requested testbed, slices need to be first added...`. 

1. To `select` a **EMBB** slice -> `POST` Request: 
- `172.28.18.101:8080/api/v1/sliceInventory/NB/selectSlice/embb`
`JSON` body:
```
{
  "expDataRateUL": 200,
  "expDataRateDL": 200,
  "areaTrafficCapUL": 200,
  "areaTrafficCapDL": 200,
  "userDensity": 200,
  "userSpeed": 80,
  "testbed": "Antwerp",
  "imsi":"imsi1"
}
```
In case there is a match for specific slice then the VITAL-5G Slice Manager & Invenotry will send out the `sliceUE/attach` request towards the corresponding local slice plugin (it requests from the VITAL-5G Multi-site inventory the `slicingAgentUrl`). In case the VITAL-5G Slice Manager & Invenotry fails to contact the local slice plugin of the specific testbed it will return in the responseEntity the HttpStatuscode `500` with the message: `Slice plugin failed to attach in a static way the imsi to the slice`. In the case that there wasn't a match for a slice, then the VITAL-5G Slice Manager & Invenotry will return in the responseEntity the HttpStatuscode `500` with the message: `No pre-provisioned slice in the db and the testbed doesn't support dynamic provisioning`. In the case that there weren't yet slices in the database then the VITAL-5G Slice Manager & Invenotry will return in the responseEntity the HttpStatuscode `500` with the message: `There are 0 slices on the requested testbed, slices need to be first added...`. 

### For the dynamical creation of a slice
As agreed is not yet supported will be supported in upcoming releases (when all the local slice plugins supports it).

### To attach an experiment to a slice [REST call from Slice Inventory to Slice Plugin]
1. When a slice has been selected on the VITAL-5G Slice Inventory and the VITAL-5G Service LCM communicates to attach the slice then it will be communicated like this towards the local Slice Plugin on the corresponding testbed: `POST` request (thus to the Slice Plugin). 
- `REST endpoint on the local Slice Plugin on the local testbed/sliceUE/attach`
`JSON` body:
```
{
  "imsi": "TercofinII",
  "sliceId": "AntwerpEmbb01",
  "usedDataRateUL": 2,
  "usedDataRateDL": 20
}
```
HttpStatuscode `200` expected if everything is succesfull, when unsuccessful: HttpStatuscode `501` expected. 
If the VITAL-5G Slice Inventory receives HttpStatuscode `200` it will comunicate towards the Service LCM that the experiment is succesfully attached towards the was succesfull. 


## Deployment/installation 
Below are the pre requirements prior to run the VITAL-5G Slice Manager and Inventory. 
### Installing PostgreSQL + Monitoring database
1. `sudo apt update` / `sudo apt upgrade
2. `sudo apt install postgresql postgresql-contrib`
3. `sudo systemctl start postgresql.service`
4. `sudo -i -u postgres`
5. `psql`
6. `CREATE DATABASE sliceinventory;`
7. `\q`
8. `psql sliceinventory`
9. `ALTER USER postgres PASSWORD 'postgres';``
Done!

### Installing Java 17 + MAVEN
1. `sudo apt install openjdk-17-jdk-headless`
2. `nano ~/.bashrc`
Provide the following at the end of the file: `# SET JAVA_HOME PATH`
```
JAVA_HOME=/usr/lib/jvm/java-17-openjdk-amd64
export JAVA_HOME
PATH=$JAVA_HOME/bin:$PATH
```
3. ` . ~/.bashrc`
4. `echo $JAVA_HOME`
5. `wget https://repo.maven.apache.org/maven2/org/apache/maven/apache-maven/3.8.6/apache-maven-3.8.6-bin.tar.gz`
6. `tar -xvf apache-maven-3.8.6-bin.tar.gz`
7. `sudo mv apache-maven-3.8.6 /opt/`
8. `sudo chown -R $USER:$USER /opt/apache-maven-3.8.6` 
9. `nano ~/.bashrc` -> Override the `# SET JAVA_HOME PATH` with the following: 
```
JAVA_HOME=/usr/lib/jvm/java-17-openjdk-amd64
MAVEN_HOME=/opt/apache-maven-3.8.6
export JAVA_HOME
export MAVEN_HOME
PATH=$JAVA_HOME/bin:$MAVEN_HOME/bin:$PATH
```
10. `. ~/.bashrc`
11. `echo $MAVEN_HOME`
12 `mvn -v` -> you should see the following: 
```
Java version: 17.0.7, vendor: Private Build, runtime: /usr/lib/jvm/java-17-openjdk-amd64
Default locale: en_GB, platform encoding: UTF-8
OS name: "linux", version: "4.15.0-184-generic", arch: "amd64", family: "unix"
```
Done!

### Running the VITAL-5G Slice Mangaer and Inventory:
Just run the JAR file you can find in the directory: JAR 
like this: `java -jar SliceInventory-0.0.1-SNAPSHOT.jar`

